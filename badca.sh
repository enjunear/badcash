#! /bin/bash
# badca.sh
# BAsic Docker-Compose Aggregator SHell script

printf '%s\n' "######################################"
printf '%s\n' "Generate a new docker-compose.yml file"

if [ -f docker-compose.yml ]; then
        printf '\n%s\n' "Backing up old docker-compose.yml"

        if [ ! -d old ]; then
                printf '\n%s\n' "Directory 'old' doesn't exist. Creating it now..."
                mkdir old
        fi

        oldfile=old/$(date +"%s")-docker-compose.yml
        mv docker-compose.yml "${oldfile}"
        printf '%s\n' "docker-compose.yml moved to ${oldfile}"
fi

printf '\n%s\n' "Finding docker-compose.yml files..."
files="$(find . -name docker-compose.yml -not -path ./old)"
printf '\n%s\n' "Found files:"
printf '%s\n' "${files}"

for file in ${files}
do
        list+=" -f ${file}"
done

printf '\n%s\n' "Generating new docker-compose.yml:"
command="docker-compose ${list} config"
printf '%s\n' "${command} > docker-compose.yml"
${command} > docker-compose.yml

printf '\n%s\n' "New docker-compose.yml file:"
printf '%s\n' "++++++++++++++++++++"
cat docker-compose.yml
printf '%s\n' "++++++++++++++++++++"
printf '\n%s\n' "Done"
printf '%s\n' "######################################"
