# BAsic Docker-Compose Aggregator SHell script

A shell script for creating a single docker-compose.yml file from multiple docker-compose.yml files.

## Setup

With your directory structure like this:
```
docker
  |____service1
  | |____docker-compose.yml
  |____service2
  | |____docker-compose.yml
  |____service3
  | |____docker-compose.yml
  |
  |...
```

## Install
Copy `badca.sh` into the `docker` folder

Give it executable permisions
```shell
chmod +x badca.sh
```

## Usage

Just run it
```shell
./badca.sh
```
